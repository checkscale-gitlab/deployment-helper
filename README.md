# deployment-helper

Simple Helper-Image to Deploy Websites to remote sftp servers.

```bash
lftp -e 'set sftp:auto-confirm yes; mirror -R ./<LOCAL_DIR>/ ./<REMOTE_DIR>/; bye' --env-password -u USER sftp://<TARGET_FQDN>
```

